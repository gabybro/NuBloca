package ro.nubloca;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;
import ro.nubloca.Network.GetRequest;
import ro.nubloca.Network.HttpBodyGet;

import static ro.nubloca.Helper.Global.ACCEPT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_TYPE;
import static ro.nubloca.Helper.Global.URL1ECRAN1;
import static ro.nubloca.Helper.Global.URL1ECRAN10;

public class Ecran10 extends BaseActivity {
    boolean isLogged = false;
    static InputStream is = null;
    static String json = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran10);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        isLogged = ((Global) this.getApplication()).getUserIsLogged();

        if (getSupportActionBar() != null) {
            /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);*/
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if (isLogged){
            TextView email = (TextView)findViewById(R.id.email);
            email.setVisibility(View.GONE);
        }

        TextView trimite = (TextView)findViewById(R.id.btn_ok);
        trimite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    //Toast.makeText(getApplicationContext(), "islogged", Toast.LENGTH_LONG).show();
                    createHandler();
                    //finish();

            }
        });


    }
    private void createHandler() {
        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        prepData();
                    }
                }, 1000);
                Looper.loop();
            }
        };
        thread.start();
    }

    private void prepData() {
        String app_code = ((Global) this.getApplication()).getApp_code();
        //TODO remove
        app_code = "bede61d1dd6115de2ad5d5f30ea03b36";

        EditText nameTxt = (EditText) findViewById(R.id.name);
        String name = nameTxt.getText().toString();
        EditText phoneTxt = (EditText) findViewById(R.id.phone);
        String phone = phoneTxt.getText().toString();
        EditText emailTxt = (EditText) findViewById(R.id.email);
        String email = emailTxt.getText().toString();
        EditText messageTxt = (EditText) findViewById(R.id.message);
        String message = messageTxt.getText().toString();
        message = "asd";


        JSONObject js = new JSONObject();
        JSONObject jsonobject_identificare = new JSONObject();
        JSONObject jsonobject_app_code = new JSONObject();
        JSONObject jsonobject_trimise = new JSONObject();

        try {
            jsonobject_app_code.put("app_code", app_code);
            jsonobject_identificare.put("user", jsonobject_app_code);
            js.put("identificare", jsonobject_identificare);



            jsonobject_trimise.put("mesaj", message);
            if (isLogged){
                int id_user = ((Global) this.getApplication()).getId_user();
                jsonobject_trimise.put("id_user", id_user);
            } else {
                jsonobject_trimise.put("email", email);
            }

            js.put("trimise", jsonobject_trimise);

            Toast.makeText(this, js.toString(), Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        GetRequest elem = new GetRequest();
        JSONObject response = elem.makeRequest(js, URL1ECRAN10);


        getData(response);

        /*{
            "identificare": {
            "user": {
                "app_code": "bede61d1dd6115de2ad5d5f30ea03b36"
            }
        },
            "trimise": {
                "id_user": -1,
                "email": "jordas.macin@gmail.com",
                "mesaj": "Mesaj de macaroane cu branza"
        }
        }*/

    }
    private void getData(JSONObject jObj) {
        String status = "xxxx";

        try {
            status = jObj.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, jObj.toString(), Toast.LENGTH_LONG).show();
    }

}
