package ro.nubloca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import ro.nubloca.Helper.BaseActivity;

public class Ecran14 extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran14);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        RelativeLayout btnUseri= (RelativeLayout)findViewById(R.id.useriBlocati);
        btnUseri.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Intent i = new Intent(getApplicationContext(),Ecran17.class);
                startActivity(i);
            }
        });

        RelativeLayout btnDelete = (RelativeLayout)findViewById(R.id.stergeCont);
        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Intent i = new Intent(getApplicationContext(),Ecran34.class);
                startActivity(i);
            }
        });

        RelativeLayout btnPass = (RelativeLayout)findViewById(R.id.schimbaParola);
        btnPass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Intent i = new Intent(getApplicationContext(),Ecran42.class);
                startActivity(i);
            }
        });

        RelativeLayout btnMasini = (RelativeLayout)findViewById(R.id.masinilemele);
        btnMasini.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Intent i = new Intent(getApplicationContext(),Ecran16.class);
                startActivity(i);
            }
        });

    }

}
