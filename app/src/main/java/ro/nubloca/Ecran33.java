package ro.nubloca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ro.nubloca.Helper.BaseActivity;

public class Ecran33 extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran33);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        TextView btnConfirma = (TextView) findViewById(R.id.btn_ok);
        if (btnConfirma != null)
            btnConfirma.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getEmail();
                    finish();
                    startActivity(new Intent(Ecran33.this, Ecran41.class));
                }
            });
    }

    private void getEmail() {
        EditText editText = (EditText) findViewById(R.id.email);
        String value = editText.getText().toString();
        Toast.makeText(this, value, Toast.LENGTH_LONG).show();
    }

}
