package ro.nubloca;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;
import ro.nubloca.Network.HttpBodyGet;

import static ro.nubloca.Helper.Global.ACCEPT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_TYPE;
import static ro.nubloca.Helper.Global.URL1ECRAN31;


public class Ecran31 extends BaseActivity {
    static InputStream is = null;
    static String json = "";
    String email, pass;
    String mesaj = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran31);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final EditText userEmail = (EditText) findViewById(R.id.email);
        final EditText userPass = (EditText) findViewById(R.id.password);

        /*phoneNumber = ((Global) getApplicationContext()).getGlobalPhoneNumber();
        TextView phoneNr = (TextView) findViewById(R.id.phoneNr);
        phoneNr.setText(phoneNumber);*/

        TextView contNou = (TextView) findViewById(R.id.newAcc);
        contNou.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Intent i = new Intent(getApplicationContext(),Ecran30.class);
                //startActivity(i);
                //Toast.makeText(getApplicationContext(), "Nope", Toast.LENGTH_LONG).show();
            }
        });

        TextView autentificare = (TextView) findViewById(R.id.acord);
        autentificare.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //// TODO: 5/3/2017 send autentification to server
                email = userEmail.getText().toString();
                pass = userPass.getText().toString();
                //if ok start ecran7
                createHandler(email, pass);

            }
        });

        TextView forgotPass = (TextView) findViewById(R.id.forgotPass);
        forgotPass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getApplicationContext(), Ecran33.class);
                startActivity(i);
            }
        });

    }

    private void dataAuth(String email, String pass) {
        //versiune_app
        String app_code = ((Global) this.getApplication()).getApp_code();

        /*Toast.makeText(getApplicationContext(), "email: "+email+"pass: "+pass,
                Toast.LENGTH_LONG).show();*/

        //TODO remove
        app_code = "bede61d1dd6115de2ad5d5f30ea03b36";
        email = "jordas.macin@gmail.com";
        pass = "macaroane";

        JSONObject js = new JSONObject();
        JSONObject jsonobject_identificare = new JSONObject();
        JSONObject jsonobject_trimise = new JSONObject();
        JSONObject jsonobject_app_code = new JSONObject();

        try {
            jsonobject_app_code.put("app_code", app_code);
            jsonobject_identificare.put("user", jsonobject_app_code);
            js.put("identificare", jsonobject_identificare);

            jsonobject_trimise.put("email", email);
            jsonobject_trimise.put("pass", pass);
            js.put("trimise", jsonobject_trimise);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*{     "identificare": {   "user": {   "app_code": "bede61d1dd6115de2ad5d5f30ea03b36"    }     },
                "trimise": {        "email": "jordas.macin@gmail.com",
                                    "pass": "macaroane"  } }*/
        HttpClient httpClient = new DefaultHttpClient();
        HttpBodyGet httpPost = new HttpBodyGet(URL1ECRAN31);
        httpPost.setHeader("Content-Type", CONTENT_TYPE);
        httpPost.setHeader("Content-Language", CONTENT_LANGUAGE);
        httpPost.setHeader("Accept-Language", ACCEPT_LANGUAGE);

        //Encoding POST data
        try {

            httpPost.setEntity(new ByteArrayEntity(js.toString().getBytes("UTF8")));

        } catch (UnsupportedEncodingException e) {
            // log exception
            e.printStackTrace();
        }

        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity result = response.getEntity();

            is = result.getContent();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            JSONObject jObj = null;
            try {
                jObj = new JSONObject(json);
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            mesaj = "";
            int id_user = -2;

            try {
                mesaj = jObj.getString("mesaj");
                id_user = jObj.getInt("id_user");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (mesaj.equals("OK")) {
                ((Global) this.getApplication()).setUserIsLogged(true);
                ((Global) this.getApplication()).setUserEmail(email);
                ((Global) this.getApplication()).setUserPass(pass);
                ((Global) this.getApplication()).setId_user(id_user);
            } else {
                //TODO appropriate msg
                Toast.makeText(this, mesaj, Toast.LENGTH_LONG).show();
            }


            //((Global) this.getApplication()).setTosText(text);
            //((Global) this.getApplication()).setApp_code(appCode);
            //((Global) this.getApplication()).setId_tcl(id_tcl);
            //Toast.makeText(this, "mesaj: " + mesaj + "id_user" + id_user, Toast.LENGTH_LONG).show();
            //Toast.makeText(this, text, Toast.LENGTH_LONG).show();

        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }
    }

    private void createHandler(final String email, final String pass) {
        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dataAuth(email, pass);
                        if (mesaj.equals("OK")) {
                            Intent i = new Intent(getApplicationContext(), Ecran7.class);
                            startActivity(i);
                        }
                    }
                }, 1000);
                Looper.loop();
            }
        };
        thread.start();
    }

}
