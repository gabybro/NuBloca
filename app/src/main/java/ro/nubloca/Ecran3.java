package ro.nubloca;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;

public class Ecran3 extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        String data = ((Global) this.getApplication()).getTosText();

        WebView webview = (WebView) this.findViewById(R.id.webview);
        webview.loadData(data, "text/html; charset=utf-8", "UTF-8");

        //textView.setText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        //textView.setText(((Global) this.getApplication()).getTosText());
    }

}
