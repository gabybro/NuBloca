package ro.nubloca;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.extras.FontTitilliumBold;

public class Ecran20 extends BaseActivity {
    int campuri = 3;
    FontTitilliumBold field;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran20);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }




        View btn_istoric_numere = (View) findViewById(R.id.istoricNumere);
        if (btn_istoric_numere != null)
            btn_istoric_numere.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Ecran20.this, Ecran24.class));

                }
            });

        View btn_cum_functioneaza = (View) findViewById(R.id.cumFunctioneaza);
        if (btn_cum_functioneaza != null)
            btn_cum_functioneaza.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                   // startActivity(new Intent(Ecran20.this, Ecran13.class));

                }
            });

        View btn2 = (View) findViewById(R.id.alegeTipInmatriculare);
        if (btn2 != null)
            btn2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                   // startActivity(new Intent(Ecran20.this, Ecran23.class));
                }
            });


        LinearLayout btn1 = (LinearLayout) this.findViewById(R.id.textSalutAiParcat);
        if (btn1 != null)
            btn1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //finish();
                    startActivity(new Intent(Ecran20.this, Ecran22.class));

                }
            });

        showElements();

    }

    private void showElements() {
        campuri = 3;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int divLength = size.x;
        int height = size.y;

        int nrUML = 9;
        int nrSPI = 2;
        int nrSPL = 2;

        int valUML = 0;
        int valSPL = 0;
        int valSPI = 0;

        int valMaxUML = divLength / 10;


        valSPI = divLength / (nrUML * 6 + 3 * nrSPL + nrSPI);
        valSPL = 3 * valSPI;
        valUML = 6 * valSPI;
        int valRealUml = Math.min(valMaxUML, valUML);
        int minTrei = 0;

        LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.rel);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear_plate_holder);
        ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT, 1f);
        //params.width = minTrei * valRealUml;
        //linearLayout.setPadding(valSPL, 10, valSPL,10);

        //ViewGroup.LayoutParams params = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT, 1f);
        /*TextView field1 = new TextView(this);
        field1.setHeight(5);
        field1.setWidth(valSPI);
        linearLayout.addView(field1);*/

        String[] spinnerArray = {"TL","BR","GL"};

        /*Spinner spinner = new Spinner(this);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,  R.layout.raw_list_1, spinnerArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter); */

        Spinner spinner = new Spinner(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.raw_list_1, spinnerArray);
        spinner.setAdapter(adapter);
        int spinnerPosition = adapter.getPosition("TL");
        spinner.setSelection(spinnerPosition);

        //params.width = minTrei * valRealUml;

        //spinner.setLayoutParams(params);
        //spinner.setBackgroundResource(R.drawable.plate_border);
        //spinner.setLayoutParams(params);

        linearLayout1.addView(spinner);

        field = new FontTitilliumBold(this);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT, 1f);
        params1.setMargins(valSPI, 0, 0, 0);
        field.setLayoutParams(params1);
        field.setWidth(valRealUml * minTrei);
        field.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER);
        field.setTextSize(25);
        field.setTextColor(getResources().getColor(R.color.text_layout_custom));
        field.setBackgroundResource(R.drawable.plate_border);
        field.setWidth(valRealUml *3);
        linearLayout.addView(field);

        field = new FontTitilliumBold(this);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayoutCompat.LayoutParams.WRAP_CONTENT, 1f);
        params1.setMargins(valSPI, 0, 0, 0);
        field.setLayoutParams(params2);
        field.setWidth(valRealUml * minTrei);
        field.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER);
        field.setTextSize(25);
        field.setTextColor(getResources().getColor(R.color.text_layout_custom));
        field.setBackgroundResource(R.drawable.plate_border);
        field.setWidth(valRealUml *3);
        linearLayout.addView(field);



    }

}
