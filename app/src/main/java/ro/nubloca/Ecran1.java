package ro.nubloca;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.ProgressBar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;
import ro.nubloca.Network.GetRequest;
import ro.nubloca.Network.HttpBodyGet;
import ro.nubloca.Network.InternetConnectionHelper;

import static ro.nubloca.Helper.Global.ACCEPT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_TYPE;
import static ro.nubloca.Helper.Global.URL1ECRAN1;
import static ro.nubloca.Helper.Global.VERSIUNE_APP;

public class Ecran1 extends BaseActivity {

    static InputStream is = null;
    private static final int REQUEST_READ_PHONE_STATE = 0;
    private ProgressBar progressBar;
    String device_nume, device_model, os_versiune, os_nume, telefon_tara, telefon_numar, phoneNumber;
    int versiune_app;
    static String json = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran1);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#fcd116"), PorterDuff.Mode.SRC_IN);

        // At activity startup we manually check the internet status
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            //Toast.makeText(this, "Your Message", Toast.LENGTH_LONG).show();
            createHandler();

            new Timer().schedule(new TimerTask() {
                public void run() {
                    Ecran1.this.runOnUiThread(new Runnable() {
                        public void run() {
                            startActivity(new Intent(Ecran1.this, Ecran30.class));
                            finish();
                        }
                    });
                }
            }, 3000);

        } else {
            Intent i = new Intent();
            i.setClassName("ro.nubloca", "ro.nubloca.Ecran0");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }

    }

    private void prepData() {

        //device
        device_nume = android.os.Build.MANUFACTURER;
        device_model = android.os.Build.MODEL;

        //os
        os_nume = "Android";
        os_versiune = Build.VERSION.RELEASE;

        //versiune_app
        versiune_app = VERSIUNE_APP;

        //telefon
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telefon_tara = tm.getSimCountryIso();

        /*{ "identificare": { "user": {} },
            "trimise": { "device": { "nume": "LG",
                                    "model": "Nexus 5" },
                        "os":      { "nume": "Android",
                                     "versiune": "12.4"},
                        "versiune_app": 1,
                        "tara": "ro" } } }*/

        JSONObject js = new JSONObject();
        JSONObject jsonobject_identificare = new JSONObject();
        JSONObject jsonobject_trimise = new JSONObject();
        JSONObject jsonobject_device = new JSONObject();
        JSONObject jsonobject_os = new JSONObject();
        JSONObject jsonobject_telefon = new JSONObject();
        try {
            jsonobject_identificare.put("user", "");

            jsonobject_device.put("nume", device_nume);
            jsonobject_device.put("model", device_model);
            jsonobject_trimise.put("device", jsonobject_device);

            jsonobject_os.put("nume", os_nume);
            jsonobject_os.put("versiune", os_versiune);
            jsonobject_trimise.put("os",jsonobject_os);

            jsonobject_trimise.put("versiune_app",versiune_app);

            jsonobject_trimise.put("tara",telefon_tara);

            js.put("identificare", jsonobject_identificare);
            js.put("trimise", jsonobject_trimise);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        GetRequest elem = new GetRequest();
        JSONObject response = elem.makeRequest(js, URL1ECRAN1);


        getData(response);

    }

    private void getData(JSONObject jObj) {

            JSONObject jtos = null;
            String appCode = "noCode";
            int id_tcl = 0;
            String tos = "";

            try {
                id_tcl = jObj.getInt("id_tcl");
                appCode = jObj.getString("app_code");
                jtos = jObj.getJSONObject("termeni_conditii");
                tos = jtos.getString("text");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ((Global) this.getApplication()).setTosText(tos);
            ((Global) this.getApplication()).setApp_code(appCode);
            ((Global) this.getApplication()).setId_tcl(id_tcl);
            //Toast.makeText(this, result, Toast.LENGTH_LONG).show();
            //Toast.makeText(this, text, Toast.LENGTH_LONG).show();

        /*{ "app_code": "420be1839e61703f22b836a51f93312e",
            "alte_instalari": 0,
            "id_nr_telefon": 1,
            "termeni_conditii":{
                    "id_tcl": 1,
                    "text": "Termeni și Condiții, descriere pe larg.\r\n<br /><br />\r\nTermeni și Condiții, descriere pe larg.\r\n<br /><br />\r\nTermeni și Condiții, descriere pe larg.\r\n<br /><br />\r\nTermeni și Condiții, descriere pe larg.",
                    "noutati": "Noutăți ale acestor Termeni\r\n<br /><br />\r\nfață de <b>Termenii</b> precedenți",
                    "data_impunerii": "2016-04-10 18:52:03"  }  }*/

    }


    private void createHandler() {
        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        prepData();
                    }
                }, 1000);
                Looper.loop();
            }
        };
        thread.start();
    }

}
