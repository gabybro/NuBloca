package ro.nubloca.Helper;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import ro.nubloca.Network.InternetConnectionHelper;

/**
 * Created by gabybro on 5/3/2017.
 */

public class BaseActivity  extends AppCompatActivity {
    @Override
    protected void onPause() {

        super.onPause();
        InternetConnectionHelper.activityPaused();// On Pause notify the Application
    }

    @Override
    protected void onResume() {

        super.onResume();
        InternetConnectionHelper.activityResumed();// On Resume notify the Application
    }


}
