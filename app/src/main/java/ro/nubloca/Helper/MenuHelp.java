package ro.nubloca.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gabybro on 5/5/2017.
 */

public class MenuHelp {
    public int id;
    public int id_parinte;
    public int nr_copii;
    public String titlu;

    public MenuHelp(JSONObject object) {
        try {
            this.id = object.getInt("id");
            this.id_parinte = object.optInt("id_parinte",0);
            this.nr_copii = object.getInt("nr_copii");
            this.titlu = object.getString("titlu");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public static ArrayList<MenuHelp> fromJson(JSONArray jsonObjects) {
        ArrayList<MenuHelp> elem = new ArrayList<MenuHelp>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                elem.add(new MenuHelp(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return elem;
    }

}