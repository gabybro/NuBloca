package ro.nubloca.Helper;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gabybro on 4/26/2017.
 */

public class Global extends Application {

    //TODO
    //public static final int BUILD_TYPE = 0; // 0 - development, 1 - test, 2 - live
    public static final int VERSIUNE_APP = 1;
    public static final String CONTENT_TYPE = "application/json";
    public static final String CONTENT_LANGUAGE = "ro";
    public static final String ACCEPT_LANGUAGE = "ro";
    /*url*/
    public static final String URL1ECRAN1 = "http://api.nubloca.ro/ecran1/request1/";
    public static final String URL1ECRAN30 = "http://api.nubloca.ro/ecran30/request1/";
    public static final String URL1ECRAN31 = "http://api.nubloca.ro/ecran31/request1/";
    public static final String URL1ECRAN10 = "http://api.nubloca.ro/ecran10/request1/";
    public static final String URL1ECRAN11 = "http://api.nubloca.ro/ecran11/request1/";
    public static final String URL1ECRAN13 = "http://api.nubloca.ro/ecran13/request1/";
    public String userEmail = "";
    int id_tcl = 0;
    private String userPass = "";
    private boolean userIsLogged = false;
    private String globalPhoneNumber;
    private String tosText;
    private String app_code;

    public List<Integer> getEcran11tree() {
        return ecran11tree;
    }

    public void setEcran11tree(List<Integer> ecran11tree) {
        this.ecran11tree = ecran11tree;
    }

    List<Integer> ecran11tree = new ArrayList<Integer>();

    public int getEcran11parinte() {
        return ecran11parinte;
    }

    public void setEcran11parinte(int ecran11parinte) {
        this.ecran11parinte = ecran11parinte;
    }

    int ecran11parinte = 0;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    int id_user=-2;

    public boolean getUserIsLogged() {
        return userIsLogged;
    }

    public void setUserIsLogged(boolean userIsLogged) {
        this.userIsLogged = userIsLogged;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }


    public String getGlobalPhoneNumber() {
        return globalPhoneNumber;
    }

    public void setGlobalPhoneNumber(String globalPhoneNumber) {
        this.globalPhoneNumber = globalPhoneNumber;
    }

    public int getId_tcl() {
        return id_tcl;
    }

    public void setId_tcl(int id_tcl) {
        this.id_tcl = id_tcl;
    }

    public String getApp_code() {
        return app_code;
    }

    public void setApp_code(String app_code) {
        this.app_code = app_code;
    }

    public String getTosText() {
        return tosText;
    }

    public void setTosText(String tosText) {
        this.tosText = tosText;
    }
}