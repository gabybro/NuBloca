package ro.nubloca;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.extras.CustomFontTitilliumRegular;

public class Ecran22 extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran22);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        String[] values = {"Ai parcat ca un bou pe loc...","Masina ta e parcata nere...","Masina ta blocheaza acc...","Mi-ai blocat masina","Masina ta blocheaza pis..."};

        ListAdapter customAdapter = new CustomAdapter(this, values);
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(customAdapter);

        LinearLayout vezi = (LinearLayout) findViewById(R.id.linear1);
        if (vezi != null)
            vezi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Ecran22.this, Ecran27.class));
                }
            });




        TextView prop = (TextView) findViewById(R.id.propune_mesaj);
        if (prop != null)
            prop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Ecran22.this, Ecran28.class));
                }
            });
    }

    public class CustomAdapter extends ArrayAdapter<String> {


        public CustomAdapter(Context context, String[] elemente) {
            super(context, R.layout.raw_list22, elemente);


        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater dapter = LayoutInflater.from(getContext());
            View customView = dapter.inflate(R.layout.raw_list22, parent, false);

            String singleFood = getItem(position);
            CustomFontTitilliumRegular textul = (CustomFontTitilliumRegular) customView.findViewById(R.id.text);
            final ImageView imaginea = (ImageView) customView.findViewById(R.id.radioButton);
            LinearLayout ll = (LinearLayout) customView.findViewById(R.id.linear1);
            LinearLayout lll = (LinearLayout) customView.findViewById(R.id.linear2);

            textul.setText(singleFood);



            if (ll != null) {
                ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
            }


            if (lll != null) {
                lll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
            }

            return customView;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
