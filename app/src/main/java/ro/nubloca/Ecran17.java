package ro.nubloca;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import ro.nubloca.Helper.BaseActivity;

public class Ecran17 extends BaseActivity {

    private SwipeMenuListView mListView;

    String[] name = {"Cristi Iordache","Valentin Stefan","Gabriel Basca","Thanatos Petru","Alonzo Phoibos","Richard Reynaldo","Severus Diodotos"};

    private ArrayList<String> mArrayList=new ArrayList<>();

    private Ecran17.ListDataAdapter mListDataAdapter;

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.ecran17);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        initControls();

        ImageView btn = (ImageView)findViewById(R.id.btn_ok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.length>mArrayList.size()){
                    mArrayList.add(name[mArrayList.size()]);
                    mListDataAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void initControls() {

        mListView=(SwipeMenuListView)findViewById(R.id.listView);

        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

       /* for (int i=0;i<5;i++){

            mArrayList.add("List item --> "+i);

        }*/
        mArrayList.add("Cristi Iordache");
        mArrayList.add("Valentin Stefan");
        mArrayList.add("Gabriel Basca");

// mListView.setCloseInterpolator(new BounceInterpolator());

        mListDataAdapter=new Ecran17.ListDataAdapter();

        mListView.setAdapter(mListDataAdapter);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override

            public void create(SwipeMenu menu) {

// Create different menus depending on the view type

               /* SwipeMenuItem goodItem = new SwipeMenuItem(

                        getApplicationContext());

// set item background

                goodItem.setBackground(new ColorDrawable(Color.rgb(0x30, 0xB1,

                        0xF5)));

// set item width

                goodItem.setWidth(dp2px(90));

// set a icon

                goodItem.setIcon(R.drawable.ic_action_good);

// add to menu

                menu.addMenuItem(goodItem);*/

// create "delete" item

                SwipeMenuItem deleteItem = new SwipeMenuItem(

                        getApplicationContext());

// set item background

                /*deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,

                        0x3F, 0x25)));*/

// set item width

                deleteItem.setWidth(dp2px(120));

// set a icon

                deleteItem.setBackground( getResources().getDrawable(R.drawable.delete));
                //deleteItem.setIcon(R.drawable.delete);

// add to menu

                menu.addMenuItem(deleteItem);

            }

        };

// set creator

        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {

            @Override

            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 1:

                        Toast.makeText(Ecran17.this,"Like button press",Toast.LENGTH_SHORT).show();

                        break;

                    case 0:

                        mArrayList.remove(position);

                        mListDataAdapter.notifyDataSetChanged();

                        Toast.makeText(Ecran17.this,"Item deleted",Toast.LENGTH_SHORT).show();

                        break;

                }

                return true;

            }

        });

//mListView

        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {

            @Override

            public void onMenuOpen(int position) {

            }

            @Override

            public void onMenuClose(int position) {

            }

        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override

            public void onSwipeStart(int position) {

            }

            @Override

            public void onSwipeEnd(int position) {

            }

        });

    }


    class ListDataAdapter extends BaseAdapter {

        Ecran17.ListDataAdapter.ViewHolder holder;

        @Override

        public int getCount() {

            return mArrayList.size();

        }

        @Override

        public Object getItem(int i) {

            return null;

        }

        @Override

        public long getItemId(int i) {

            return 0;

        }

        @Override

        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null){

                holder=new Ecran17.ListDataAdapter.ViewHolder();

                convertView=getLayoutInflater().inflate(R.layout.list_item,null);

                holder.mTextview=(TextView)convertView.findViewById(R.id.textView);

                convertView.setTag(holder);

            }else {

                holder= (Ecran17.ListDataAdapter.ViewHolder) convertView.getTag();

            }

            holder.mTextview.setText(mArrayList.get(position));

            return convertView;

        }

        class ViewHolder {

            TextView mTextview;

        }

    }

    private int dp2px(int dp) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,

                getResources().getDisplayMetrics());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            //TODO add item to list from here
            mArrayList.add("List item --> "+mArrayList.size());
            mListDataAdapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }

}