package ro.nubloca;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;
import ro.nubloca.Helper.MenuHelp;
import ro.nubloca.Network.GetRequest;
import ro.nubloca.Network.HttpBodyGet;

import static ro.nubloca.Helper.Global.ACCEPT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_TYPE;
import static ro.nubloca.Helper.Global.URL1ECRAN11;
import static ro.nubloca.Helper.Global.URL1ECRAN13;

public class Ecran11 extends BaseActivity {
    ArrayList<MenuHelp> newUsers;
    int parinte = 0 ;
    UsersAdapter adapter;
    int current = 0;
    List<Integer> parinteTree = new ArrayList<Integer>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran11);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
       // parinte = ((Global) this.getApplication()).getEcran11parinte();
       // Toast.makeText(getApplicationContext(), "start", Toast.LENGTH_LONG).show();
        //parinteTree.add(0);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    screen();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // Construct the data source
        ArrayList<MenuHelp> arrayOfUsers = new ArrayList<MenuHelp>();
        // Create the adapter to convert the array to views
        adapter = new UsersAdapter(this, arrayOfUsers);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(adapter);
        adapter.addAll(newUsers);


    }




    private void screen() throws JSONException {
        /*{"identificare": {            "user": {                "app_code": "bede61d1dd6115de2ad5d5f30ea03b36"            }        },
            "trimise": {            "id_parinte": null        }        }*/
        String app_code = ((Global) this.getApplication()).getApp_code();
        JSONObject user = new JSONObject().put("app_code", app_code);
        JSONObject identificare = new JSONObject().put("user", user);
        JSONObject js = new JSONObject().put("identificare", identificare);
        JSONObject trimise;

        if (parinte == 0) {
            JSONObject id_parinte = null;
            trimise = new JSONObject().put("id_parinte", id_parinte);
        } else {
           // parinteTree.add(parinte);
            int id_parinte = parinte;
            trimise = new JSONObject().put("id_parinte", id_parinte);
        }

        js.put("trimise", trimise);

        GetRequest elem = new GetRequest();
        JSONArray jsArr = elem.makeRequestArray(js, URL1ECRAN11);

        /*id": 1,
        "id_parinte": null,
        "titlu": "Cum funcționează",
        "nr_copii": 2*/


        newUsers = MenuHelp.fromJson(jsArr);


    }


    public class UsersAdapter extends ArrayAdapter<MenuHelp> {
        public UsersAdapter(Context context, ArrayList<MenuHelp> menuHelp) {
            super(context, 0, menuHelp);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            MenuHelp menuHelp = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);
            }
            // Lookup view for data population
            TextView titlu = (TextView) convertView.findViewById(R.id.textView);

            // Populate the data into the template view using the data object
            titlu.setText(menuHelp.titlu);

            RelativeLayout btButton = (RelativeLayout) convertView.findViewById(R.id.rel_bar);
            // Cache row position inside the button using `setTag`
            btButton.setTag(position);
            // Attach the click event handler
            btButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    int position = (Integer) view.getTag();
                    // Access the row position here to get the correct data item
                    // Do what you want here...
                    MenuHelp user = getItem(position);
                    parinteTree.add(parinte);
                    parinte = user.id;
                    //Toast.makeText(getApplicationContext(), parinteTree+"", Toast.LENGTH_LONG).show();
                    if (user.nr_copii == 0) {
                        //((Global) getApplication()).setEcran11tree(pAr);
                        //finish();
                        Intent i = new Intent(Ecran11.this,Ecran13.class);
                        i.putExtra("id", user.id);
                        startActivity(i);
                    }
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                screen();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    t.start();
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    adapter.clear();
                    adapter.addAll(newUsers);
                }
            });




            // Return the completed view to render on screen
            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
        if (parinteTree.size() == 0) {
            super.onBackPressed();
        } else {
            parinte = parinteTree.get(parinteTree.size() - 1);
            parinteTree.remove(parinteTree.size() - 1);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        screen();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            adapter.clear();
            adapter.addAll(newUsers);

            return;
            //super.onBackPressed();
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        parinte = parinteTree.get(parinteTree.size()-1);
        parinteTree.remove(parinteTree.size() - 1);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    screen();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        adapter.clear();
        adapter.addAll(newUsers);
    }


}
