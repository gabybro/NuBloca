package ro.nubloca.Network;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by GABY_ on 28.05.2016.
 */
public class Response {
    public String getApp_code() {
        return app_code;
    }

    public void setApp_code(String app_code) {
        this.app_code = app_code;
    }

    @SerializedName("app_code")
    private String app_code;

    public JSONObject getTermeni_conditii() {
        return termeni_conditii;
    }

    public void setTermeni_conditii(JSONObject termeni_conditii) {
        this.termeni_conditii = termeni_conditii;
    }

    @SerializedName("termeni_conditii")
    private JSONObject termeni_conditii;


}