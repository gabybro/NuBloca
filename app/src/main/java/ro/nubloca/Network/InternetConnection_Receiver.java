package ro.nubloca.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnection_Receiver extends BroadcastReceiver {
    public InternetConnection_Receiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        try {

            boolean isVisible = InternetConnectionHelper.isActivityVisible();// Check if activity is visible or not
            //Log.i("Activity is Visible ", "Is activity visible : " + isVisible);


            // If it is visible then trigger the task else do nothing
            if (isVisible == true) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager
                        .getActiveNetworkInfo();

                // Check internet connection and accrding to state change the text of activity by calling method
                /*if (networkInfo != null && networkInfo.isConnected()) {
                    // new Ecran30().changeTextStatus(true);
                    //Toast.makeText(context, "internet", Toast.LENGTH_LONG).show();
                    //To-do close Ecran0 from here
                    Intent i = new Intent();
                    i.setClassName("ro.nubloca", "ro.nubloca.Ecran0");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);

                } else {
                    // new Ecran30().changeTextStatus(false);

                    Intent i = new Intent();
                    i.setClassName("ro.nubloca", "ro.nubloca.Ecran0");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }*/

                Intent i = new Intent();
                i.setClassName("ro.nubloca", "ro.nubloca.Ecran0");
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}