package ro.nubloca;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import ro.nubloca.Helper.BaseActivity;

public class Ecran7 extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran7slider);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.logo);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        LinearLayout contactProprietarAuto = (LinearLayout)findViewById(R.id.contactProprietarAuto);
        contactProprietarAuto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Intent i = new Intent(getApplicationContext(),Ecran20.class);
                startActivity(i);
            }
        });

        LinearLayout linearLayout1 = (LinearLayout)findViewById(R.id.linearLayout1);
        linearLayout1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                Intent i = new Intent(getApplicationContext(),Ecran16.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //ajutor
            Intent i = new Intent(getApplicationContext(),Ecran11.class);
            startActivity(i);

        } else if (id == R.id.nav_gallery) {
            //contact
            Intent i = new Intent(getApplicationContext(),Ecran10.class);
            startActivity(i);

        } else if (id == R.id.despre) {

            Intent i = new Intent(getApplicationContext(),Ecran9.class);
            startActivity(i);

        } else if (id == R.id.nav_manage) {
            //termeni si conditii
            Intent i = new Intent(getApplicationContext(),Ecran3.class);
            startActivity(i);

        } else if (id == R.id.nav_share) {
            //share

        } else if (id == R.id.contul) {
            Intent i = new Intent(getApplicationContext(),Ecran14.class);
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
