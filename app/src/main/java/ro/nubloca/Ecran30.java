package ro.nubloca;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;
import ro.nubloca.Network.HttpBodyGet;

import static ro.nubloca.Helper.Global.ACCEPT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_TYPE;
import static ro.nubloca.Helper.Global.URL1ECRAN30;

public class Ecran30 extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static InputStream is = null;
    static String json = "";
    public boolean isFirstStart;
    public String phoneNumber;
    Toolbar toolbar;
    String countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran30slider);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getApplicationContext(),Ecran3.class);
                startActivity(i);
            }
        });

        TextView btnTrimite = (TextView)findViewById(R.id.btn_ok);
        btnTrimite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createHandler();
                finish();
                Intent i = new Intent(Ecran30.this, Ecran31.class);
                startActivity(i);
            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Ecran30 App Initialize SharedPreferences
                SharedPreferences getSharedPreferences = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                isFirstStart = getSharedPreferences.getBoolean("firstStart", true);
                //phoneNumber = getSharedPreferences.getString("phoneNumber", "07xx.xxx.xxx");
                /*phoneNumber = ((Global) getApplicationContext()).getGlobalPhoneNumber();
                TextView phoneNr = (TextView) findViewById(R.id.phoneNr);
                phoneNr.setText(phoneNumber);*/
                countryCode = getSharedPreferences.getString("countryCode", "RO-def");





                //  Check either activity or app is open very first time or not and do action
                if (isFirstStart) {

                    //  Launch application introduction screen
                    Intent i = new Intent(Ecran30.this, Ecran5.class);
                    startActivity(i);
                    SharedPreferences.Editor e = getSharedPreferences.edit();
                    e.putBoolean("firstStart", false);
                    e.apply();


                } else {

                    finish();
                    Intent i = new Intent(Ecran30.this, Ecran31.class);
                    startActivity(i);

                }
            }
        });
        t.start();
        try {
            t.join();
            //Toast.makeText(this, countryCode, Toast.LENGTH_LONG).show();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            //ajutor
            Intent i = new Intent(getApplicationContext(),Ecran11.class);
            startActivity(i);

        } else if (id == R.id.nav_gallery) {
            //contact
            Intent i = new Intent(getApplicationContext(),Ecran10.class);
            startActivity(i);

        } else if (id == R.id.despre) {

            Intent i = new Intent(getApplicationContext(),Ecran9.class);
            startActivity(i);

        } else if (id == R.id.nav_manage) {
            //termeni si conditii
            Intent i = new Intent(getApplicationContext(),Ecran3.class);
            startActivity(i);

        } else if (id == R.id.nav_share) {
            //share

        } else if (id == R.id.contul) {
            Intent i = new Intent(getApplicationContext(),Ecran14.class);
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getData() {


        //get app_code from shared pref or global
        String app_code = "noappcode";
        app_code = ((Global) this.getApplication()).getApp_code();
        //Toast.makeText(getApplicationContext(), app_code, Toast.LENGTH_LONG).show();

        //trimise
        //nume from edit text
        String nume = "Mr Hacker";
        EditText emn = (EditText)findViewById(R.id.nume);
        nume = emn.getText().toString();
        //pass from edit text
        String pass = "macaroane";
        EditText emp = (EditText)findViewById(R.id.parola);
        pass = emp.getText().toString();
        //id_tcl
        int id = 1;
        id = ((Global) this.getApplication()).getId_tcl();
        //nr tel
        String tel = "00000000";
        tel = "0752.173.148";
        EditText edt = (EditText)findViewById(R.id.telefon);
        tel = edt.getText().toString();
        //email
        String email = "a@a.com";
        email = "jordas.macin@gmail.com";
        EditText eml = (EditText)findViewById(R.id.email);
        email = eml.getText().toString();

        sendJsonData(app_code,nume,pass,id,tel,email);
    }

    private void sendJsonData(String appCode, String nume, String pass, int id, String tel, String email) {

        JSONObject js = new JSONObject();
        JSONObject jsonobject_identificare = new JSONObject();
        JSONObject jsonobject_appcode = new JSONObject();
        JSONObject jsonobject_trimise = new JSONObject();

        try {
            jsonobject_appcode.put("app_code",appCode);
            jsonobject_identificare.put("user", jsonobject_appcode);

            jsonobject_trimise.put("nume",nume);

            jsonobject_trimise.put("pass",pass);

            jsonobject_trimise.put("id_tcl",id);

            jsonobject_trimise.put("telefon",tel);

            jsonobject_trimise.put("email",email);

            js.put("identificare", jsonobject_identificare);
            js.put("trimise", jsonobject_trimise);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*{	"identificare": {		"user": {			"app_code": "71df2c972b898087bcd5b0160623e624"		}	},
	        "trimise": {"nume": "Cristi Iordache", "pass": "macaroane",  "id_tcl": 1, "id_nr_telefon": 1 } }*/

        HttpClient httpClient = new DefaultHttpClient();
        HttpBodyGet httpPost = new HttpBodyGet(URL1ECRAN30);
        httpPost.setHeader("Content-Type", CONTENT_TYPE);
        httpPost.setHeader("Content-Language", CONTENT_LANGUAGE);
        httpPost.setHeader("Accept-Language", ACCEPT_LANGUAGE);

        //Encoding POST data
        try {

            httpPost.setEntity(new ByteArrayEntity(js.toString().getBytes("UTF8")));

        } catch (UnsupportedEncodingException e) {
            // log exception
            e.printStackTrace();
        }


        //Toast.makeText(getApplicationContext(), js.toString(), Toast.LENGTH_LONG).show();



        //making POST request.
        try {
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity result = response.getEntity();

            is = result.getContent();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            JSONObject jObj=null;
            try {
                jObj = new JSONObject(json);
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }

            int id_user = 0;

            try {
                id_user = jObj.getInt("id_user");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //Toast.makeText(getApplicationContext(), id_user+"", Toast.LENGTH_LONG).show();
            //Toast.makeText(this, text, Toast.LENGTH_LONG).show();

        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }


    }

    private void createHandler() {
        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getData();
                    }
                }, 1000);
                Looper.loop();
            }
        };
        thread.start();
    }
}
