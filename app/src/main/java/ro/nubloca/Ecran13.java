package ro.nubloca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import ro.nubloca.Helper.BaseActivity;
import ro.nubloca.Helper.Global;
import ro.nubloca.Network.GetRequest;
import ro.nubloca.Network.HttpBodyGet;

import static ro.nubloca.Helper.Global.ACCEPT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_LANGUAGE;
import static ro.nubloca.Helper.Global.CONTENT_TYPE;
import static ro.nubloca.Helper.Global.URL1ECRAN1;
import static ro.nubloca.Helper.Global.URL1ECRAN11;
import static ro.nubloca.Helper.Global.URL1ECRAN13;

public class Ecran13 extends BaseActivity {
    String text = "";
    String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran13);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    screen();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /*TextView tv = (TextView)findViewById(R.id.textView2);
        tv.setText(text);*/
        TextView tv1 = (TextView)findViewById(R.id.textView1);
        tv1.setText(title);
        WebView webview = (WebView) this.findViewById(R.id.webview);
        webview.loadData(text, "text/html; charset=utf-8", "UTF-8");

    }

    private void screen() throws JSONException {
        /*{"identificare": {            "user": {                "app_code": "bede61d1dd6115de2ad5d5f30ea03b36"            }        },
            "trimise": {            "id": 3        }        }*/
        String app_code = ((Global) this.getApplication()).getApp_code();
        JSONObject user = new JSONObject().put("app_code", app_code);
        JSONObject identificare = new JSONObject().put("user", user);
        JSONObject js = new JSONObject().put("identificare", identificare);
        JSONObject trimise;

        int id = (getIntent().getExtras().getInt("id"));
        trimise = new JSONObject().put("id", id);


        js.put("trimise", trimise);

        GetRequest elem = new GetRequest();
        JSONArray jsArr = elem.makeRequestArray(js, URL1ECRAN13);

        text = jsArr.getJSONObject(0).getString("text");
        title = jsArr.getJSONObject(0).getString("titlu");
        /*id": 3,
        "id_parinte": 1,
        "titlu": "Cum funcționează",
        "text": "Text HTML\r\n<br /><br />\r\n<b>Principii generale</b>"*/

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
