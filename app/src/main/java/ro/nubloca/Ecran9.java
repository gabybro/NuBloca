package ro.nubloca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import ro.nubloca.Helper.BaseActivity;

public class Ecran9 extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecran9);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);*/
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }


        RelativeLayout ajutor_btn = (RelativeLayout) this.findViewById(R.id.rel_bar1);
        if (ajutor_btn != null) {

            ajutor_btn.setOnClickListener(new View.OnClickListener() {
                //Contact
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), Ecran11.class));
                }
            });
        }

        RelativeLayout contact_btn = (RelativeLayout) this.findViewById(R.id.rel_bar2);
        if (contact_btn != null) {
            contact_btn.setOnClickListener(new View.OnClickListener() {
                //Contact
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), Ecran10.class));
                }
            });
        }

        RelativeLayout btnTermeni = (RelativeLayout) this.findViewById(R.id.rel_bar3);
        if (btnTermeni != null) {
            btnTermeni.setOnClickListener(new View.OnClickListener() {
                //Contact
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), Ecran3.class));
                }
            });
        }

        ImageView btn = (ImageView)findViewById(R.id.imageView);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getApplicationContext(), MainActivity999.class));
            }
        });

    }

}
